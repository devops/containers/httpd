#
# Apache 2.4/CentOS 7 base image with Shibboleth support
#
FROM gitlab-registry.oit.duke.edu/devops/containers/centos/7:latest

RUN set -eux; \
	curl -s -o /etc/yum.repos.d/shibboleth.repo -d platform=CentOS_7 \
	  https://shibboleth.net/cgi-bin/sp_repo.cgi; \
	yum -y update; \
	yum -y upgrade; \
	yum -y install \
	httpd \
	httpd-devel \
	httpd-tools \
	mod_ssl \
	mod_xsendfile \
	shibboleth \
	; \
	yum clean all; \
	rm -rf /var/cache/yum

WORKDIR /etc/httpd

RUN set -eux; \
	# Redirect logs to stderr and stdout
	sed -i 's,^ErrorLog .*,ErrorLog /proc/self/fd/2,' conf/httpd.conf; \
	sed -i -r 's,^\s*CustomLog .*,CustomLog /proc/self/fd/1 combined,' conf/httpd.conf; \
	# Split off LoadModule directive from rest of default mod_shib config
	echo 'LoadModule mod_shib /usr/lib64/shibboleth/mod_shib_24.so' > conf.modules.d/00-shib.conf; \
	sed -i '/^LoadModule mod_shib/d' conf.d/shib.conf

# This volume enables sharing of the Shibboleth-SP socket
# with a shibboleth container via a Docker volume
VOLUME /var/run/shibboleth

# Default Apache 2.4 / CentOS 7 DocumentRoot
VOLUME /var/www/html

EXPOSE 80 443

HEALTHCHECK CMD ncat -z localhost 80

COPY ./httpd-foreground /usr/local/bin/

RUN chmod u+x /usr/local/bin/httpd-foreground

CMD [ "httpd-foreground" ]
