# Apache 2.4 container base image with Shibboleth support

This image provides Apache 2.4 server running in the foreground.
mod_shib is installed and loaded by default to support a Shibboleth service provider.
