#!/bin/bash -e

code=1
cid=$(docker run --rm -d ${build_tag:-httpd})

SECONDS=0
while [[ $SECONDS -lt 60 ]]; do
    status="$(docker inspect -f '{{.State.Health.Status}}' $cid)"
    echo "Status: $status"

    if [[ $status == "healthy" ]]; then
	code=0
	break
    fi
    [[ $status == "unhealthy" ]] && break
    sleep 15
done

docker stop $cid
exit $code
